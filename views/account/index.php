<?php
/**
 * Created 1:10 PM 6/4/18
 * @author Vladimir Votinov <vvotinov@enaza.ru>
 */

use app\models\TransactionForm;
use yii\grid\GridView;
use \app\widgets\TransferFormWidget;

/** @var $this yii\web\View */
/** @var $transactionForm TransactionForm */
/** @var $recipients array */

$this->title = 'Account';

?>

<h1>History</h1>

<?= GridView::widget([
    'dataProvider' => $transactionDataProvider,
    'columns' => [
        'id',
        'source' => 'source.username',
        'target' => 'target.username',
        'amount',
    ],
])
?>

<h2>Transfer form</h2>

<?= TransferFormWidget::widget([
        'model' => $transactionForm,
        'recipients' => $recipients,
]) ?>