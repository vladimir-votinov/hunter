<?php

/* @var $this yii\web\View */

use yii\grid\GridView;

$this->title = 'Dashboard';

echo GridView::widget([
        'dataProvider' => $userDataProvider,
]);