<?php
/**
 * Created 3:10 PM 6/4/18
 * @author Vladimir Votinov <vvotinov@enaza.ru>
 */

namespace app\widgets;

use app\models\TransactionForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Widget;

class TransferFormWidget extends Widget
{
    /** @var TransactionForm */
    public $model;

    /** @var string Form submit action url */
    public $formAction;

    /** @var array List of available recipients */
    public $recipients;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $form = ActiveForm::begin([
            'id' => 'transfer-form-' . $this->id
        ]);

        echo $form->field($this->model, 'from')->hiddenInput()->label(false);
        echo $form->field($this->model, 'to')->dropDownList($this->recipients);
        echo $form->field($this->model, 'amount')->textInput();
        echo Html::submitButton('Submit', ['class'=> 'btn btn-primary']);

        ActiveForm::end();
    }
}