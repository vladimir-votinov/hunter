<?php

use yii\db\Migration;

/**
 * Class m180604_063227_users
 */
class m180604_063227_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(10),
            'username' => $this->string(100)->notNull()->unique(),
            'balance' => $this->float(2),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
