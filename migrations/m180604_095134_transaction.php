<?php

use yii\db\Migration;

/**
 * Class m180604_095134_transaction
 */
class m180604_095134_transaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('transaction', [
            'id' => $this->primaryKey(10),
            'from' => $this->integer(10)->notNull(),
            'to' => $this->integer(10)->notNull(),
            'amount' => $this->float(2)->notNull(),
        ]);

        $this->addForeignKey('transaction-from-user', 'transaction', ['from'], 'users', ['id']);
        $this->addForeignKey('transaction-to-user', 'transaction', ['to'], 'users', ['id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('transaction-from-user', 'transaction');
        $this->dropForeignKey('transaction-to-user', 'transaction');

        $this->dropTable('transaction');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180604_095134_transaction cannot be reverted.\n";

        return false;
    }
    */
}
