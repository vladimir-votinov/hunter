<?php
/**
 * Created 12:37 PM 6/4/18
 * @author Vladimir Votinov <vvotinov@enaza.ru>
 */

namespace app\controllers;

use app\models\Transaction;
use app\models\TransactionForm;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class AccountController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'pay' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Перевод средству другому пользователю
     */
    public function actionPay()
    {
        return $this->render('index');
    }

    /**
     * Личный кабинет
     */
    public function actionIndex()
    {
        $transactionDataProvider = new ActiveDataProvider([
            'query' => Transaction::find()->own(),
        ]);

        $transactionForm = new TransactionForm();
        $transactionForm->setFrom(\Yii::$app->user->getId());

        $recipients = User::find()
            ->select('username')
            ->where(['!=', 'id', \Yii::$app->user->getId()])
            ->indexBy('id')
            ->asArray()
            ->column();

        if ($transactionForm->load(\Yii::$app->request->post()) && $transactionForm->save()) {
            $this->redirect(['index']);
        }

        return $this->render('index', compact('transactionDataProvider', 'transactionForm', 'recipients'));
    }
}