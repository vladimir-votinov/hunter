<?php
/**
 * Created 3:23 PM 6/4/18
 * @author Vladimir Votinov <vvotinov@enaza.ru>
 */

namespace app\models;

use yii\helpers\ArrayHelper;

class TransactionForm extends FormModel
{
    private $from;
    private $to;
    private $amount;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['from', 'to'], 'number'],
            [['from', 'to'], 'required'],
            ['amount', 'validateAmount']
        ]);
    }

    public function validateAmount($attribute)
    {
        if (User::getBalance($this->from) - $this->amount < -1000) {
            $this->addError($attribute, 'Invalid amount');
        }
    }

    /**
     * @param mixed $from
     * @return TransactionForm
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $to
     * @return TransactionForm
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return TransactionForm
     */
    public function setAmount($amount)
    {
        $this->amount = abs((float)$amount);
        return $this;
    }

    public function saveInternal()
    {
        $transactionBalance = new Transaction();

        $transactionBalance->from = (int)$this->from;
        $transactionBalance->to = (int)$this->to;
        $transactionBalance->amount = $this->amount;

        $transactionDb = \Yii::$app->db->beginTransaction();

        try {
            if ($transactionBalance->save()) {
                /** @var User $transmitUser */
                $transmitUser = User::findOne($transactionBalance->from);
                /** @var User $receiveUser */
                $receiveUser = User::findOne($transactionBalance->to);

                if ($transmitUser->subBalance($transactionBalance->amount) && $receiveUser->addBalance($transactionBalance->amount)) {
                    $transactionDb->commit();
                    return true;
                }
            }

            $transactionDb->rollBack();
        } catch (\Exception|\Throwable $e) {
            $transactionDb->rollBack();
        }

        return false;
    }
}