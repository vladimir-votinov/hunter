<?php
/**
 * Created 2:46 PM 6/4/18
 * @author Vladimir Votinov <vvotinov@enaza.ru>
 */

namespace app\models;


use yii\db\ActiveQuery;

class TransactionQuery extends ActiveQuery
{
    public function own()
    {
        return $this->andWhere([
            'or',
            ['from' => \Yii::$app->user->id,],
            ['to' => \Yii::$app->user->id,],
        ]);
    }

    public function transmit()
    {
        return $this->andWhere([
            'from' => \Yii::$app->user->id,
        ]);
    }

    public function receive()
    {
        return $this->andWhere([
            'to' => \Yii::$app->user->id,
        ]);
    }
}