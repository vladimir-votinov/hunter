<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\db\Expression;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    const DEFAULT_ACCOUNT_VALUE = 0;

    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne([
            'username' => $username,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public static function create($username)
    {
        $user = new static();
        $user->username = $username;
        $user->balance = static::DEFAULT_ACCOUNT_VALUE;

        if (!$user->save()) {
            throw new \RuntimeException('Can`t create new user');
        }

        return $user;
    }

    public static function exists($username)
    {
        return static::find()->where(['username' => $username])->exists();
    }

    /**
     * Метод получения баланса пользователя
     *
     * @param $userId
     * @return false|null|string
     */
    public static function getBalance($userId)
    {
        return static::find()
            ->where(['id' => $userId])
            ->select('balance')
            ->scalar();
    }

    /**
     * Уменьшение баланса пользователя
     *
     * @param $amount
     * @return bool
     */
    public function subBalance($amount)
    {
        return $this->updateAttributes([
            'balance' => new Expression('[[balance]]-:am', [':am' => $amount]),
        ]) == 1;
    }

    /**
     * Увеличение баланса пользователя
     *
     * @param $amount
     * @return bool
     */
    public function addBalance($amount)
    {
        return $this->updateAttributes([
            'balance' => new Expression('[[balance]]+:am', [':am' => $amount]),
        ]) == 1;
    }
}
