<?php
/**
 * Created 4:01 PM 6/4/18
 * @author Vladimir Votinov <vvotinov@enaza.ru>
 */

namespace app\models;


use yii\base\Model;
use yii\helpers\ArrayHelper;

class UserForm extends Model
{
    private $username;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['username'], 'uniqueUsername']
        ]);
    }

    public function uniqueUsername($attribute, $params)
    {
        if (User::exists($attribute)) {
            $this->addError('username', 'Ununique field value');
        }
    }

    /**
     * @param mixed $username
     * @return UserForm
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }
}