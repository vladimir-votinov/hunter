<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\ModelEvent;
use yii\db\AfterSaveEvent;

/**
 * FormModel
 *
 */
abstract class FormModel extends \yii\base\Model
{

    /**
     * The name of create scenario
     */
    const SCENARIO_CREATE = 'create';

    /**
     * The name of update scenario
     */
    const SCENARIO_UPDATE = 'update';

    /**
     * @event ModelEvent an event that is triggered before saving a form.
     * You may set [[ModelEvent::isValid]] to be false to stop the saving.
     */
    const EVENT_BEFORE_SAVE = 'beforeSave';

    /**
     * @event AfterSaveEvent an event that is triggered after a record is inserted.
     */
    const EVENT_AFTER_SAVE = 'afterSave';

    /**
     * This method is called at the beginning of inserting or updating a record.
     * The default implementation will trigger an [[EVENT_BEFORE_SAVE]].
     * When overriding this method, make sure you call the parent implementation like the following:
     *
     * ```php
     * public function beforeSave()
     * {
     *     if (parent::beforeSave()) {
     *         // ...custom code here...
     *         return true;
     *     } else {
     *         return false;
     *     }
     * }
     * ```
     *
     * @return boolean whether the insertion or updating should continue.
     * If false, the insertion or updating will be cancelled.
     */
    public function beforeSave()
    {
        $event = new ModelEvent;
        $this->trigger(self::EVENT_BEFORE_SAVE, $event);
        return $event->isValid;
    }

    /**
     * This method is called at the end of inserting or updating a record.
     * The default implementation will trigger an [[EVENT_AFTER_SAVE]]. The event class used is [[AfterSaveEvent]].
     * When overriding this method, make sure you call the parent implementation so that
     * the event is triggered.
     */
    public function afterSave()
    {
        $this->trigger(self::EVENT_AFTER_SAVE);
    }

    /**
     * Save a form.
     *
     * This method performs the following steps in order:
     *
     * 1. call [[beforeValidate()]] when `$runValidation` is true. If [[beforeValidate()]]
     *    returns `false`, the rest of the steps will be skipped;
     * 2. call [[afterValidate()]] when `$runValidation` is true. If validation
     *    failed, the rest of the steps will be skipped;
     * 3. call [[beforeSave()]]. If [[beforeSave()]] returns `false`,
     *    the rest of the steps will be skipped;
     * 4. call [[saveInternal()]]. If [[saveInternal()]] returns `false`,
     *    the rest of the steps will be skipped;
     * 5. call [[afterSave()]];
     *
     * In the above step 1, 2, 3 and 5, events [[EVENT_BEFORE_VALIDATE]],
     * [[EVENT_AFTER_VALIDATE]], [[EVENT_BEFORE_SAVE]], and [[EVENT_AFTER_SAVE]]
     * will be raised by the corresponding methods.
     *
     * @param boolean $runValidation whether to perform validation (calling [[validate()]])
     * before saving the record. Defaults to `true`. If the validation fails, the record
     * will not be saved to the database and this method will return `false`.
     * @param array $attributes list of attributes that need to be saved. Defaults to null,
     * meaning all attributes will be saved.
     * @return boolean whether the attributes are valid and the form is saved successfully.
     */
    public function save($runValidation = true, $attributes = null)
    {
        if ($runValidation && !$this->validate($attributes)) {
            Yii::info('FormModel not saved due to validation error.', __METHOD__);
            return false;
        }

        if (!$this->beforeSave()) {
            return false;
        }

        if ($this->saveInternal()) {
            $this->afterSave();
            return true;
        }

        return false;
    }

    /**
     * Save form logic
     */
    abstract protected function saveInternal();
}
