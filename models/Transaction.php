<?php
/**
 * Created 12:48 PM 6/4/18
 * @author Vladimir Votinov <vvotinov@enaza.ru>
 */

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Transaction
 *
 * @property integer $from
 * @property integer $to
 * @property float $amount
 *
 * @package app\models
 */
class Transaction extends ActiveRecord
{
    public static function find()
    {
        return new TransactionQuery(get_called_class());
    }

    public function getSource()
    {
        return $this->hasOne(User::class, ['id' => 'from']);
    }

    public function getTarget()
    {
        return $this->hasOne(User::class, ['id' => 'to']);
    }
}